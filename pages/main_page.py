import allure
from selenium.webdriver.common.by import By

from locators.main_page_locators import BannerPageLocators
from pages.base_page import BasePage

class Banner(BasePage):
    locators = BannerPageLocators()

    def get_banner_name(self):
        banner_name = self.element_is_visible(self.locators.BANNER_NAME).text
        return banner_name