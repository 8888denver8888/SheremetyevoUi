import time

import allure
from selenium.webdriver import Keys

from locators.services_page_locators import SearchPageLocators
from pages.base_page import BasePage

class Search(BasePage):
    locators = SearchPageLocators()

    def search_services(self):
        services = self.element_is_visible(self.locators.SERVICES).text
        self.element_is_visible(self.locators.SEARCH).send_keys(services + Keys.ENTER)
        time.sleep(1)
        return services

    def result_search_services(self):
        result_search = self.element_is_visible(self.locators.RESULT_SEARCH).text
        return result_search