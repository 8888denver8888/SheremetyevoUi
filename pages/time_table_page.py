import random
import time

import allure
from selenium.webdriver import Keys
from locators.time_table_page_locators import *
from pages.base_page import BasePage


class Search(BasePage):
    locators = SearchPageLocators()
    search_text = "Самара"

    def search_by_city(self, search_text):
        self.element_is_clickable(self.locators.SEARCH).send_keys(search_text + Keys.ENTER)
        time.sleep(1)
        return search_text

    def search_output_city(self, search_text):
        cities = self.element_are_presents(self.locators.CITY)

        for city in cities:
            city_text = city.text
            assert city_text == search_text, "The city in the search does not match the cities found"


class Flight(BasePage):
    locators = FlightPageLocators()

    def choosing_random_flight(self):
        flight = self.element_are_presents(self.locators.FLIGHT)
        random_flight = random.choice(flight)
        random_flight.click()

    def click_notification(self):
        notification = self.element_is_clickable(self.locators.NOTIFICATION)
        notification.click()
        return notification.text
