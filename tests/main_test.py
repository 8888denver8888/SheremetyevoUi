import time

import allure

from pages.main_page import *
from conftest import driver

@allure.suite("Main")
class TestMain:

    @allure.feature("Banner")
    class TestBanner:

        @allure.title("Check BannerName")
        def test_banner_name(self, driver):
            test_banner = Banner(driver, "/ru/main")
            test_banner.open()
            banner_name = test_banner.get_banner_name()
            assert banner_name == "Шереметьево", "Name in the banner is incorrect"
