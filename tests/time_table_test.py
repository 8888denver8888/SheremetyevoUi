import allure

from pages.time_table_page import *
from conftest import driver

@allure.suite("TimeTable")
class TestTimeTable:

    @allure.feature("Search")
    class TestSearch:

        @allure.title("Check ResultSearch")
        def test_search_time_table(self, driver):
            test_search_time_table = Search(driver, "/ru/timetable/")
            test_search_time_table.open()
            search_text = test_search_time_table.search_by_city()
            test_search_time_table.search_output_city(search_text)

    @allure.feature("Flight")
    class TestFlight:

        @allure.title("Check Notification")
        def test_notification(self, driver):
            test_notification = Flight(driver, "/ru/timetable/")
            test_notification.open()
            test_notification.choosing_random_flight()
            notification = test_notification.click_notification()
            assert notification == "Отменить уведомления", "Notifications are not activated"
