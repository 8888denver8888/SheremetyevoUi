import allure

from pages.services_page import *
from conftest import driver
@allure.suite("Services")
class TestServices:

    @allure.feature("Search")
    class TestSearch:

        @allure.title("Check ResultSearch")
        def test_search_services(self, driver):
            test_search = Search(driver, "/ru/services/")
            test_search.open()
            services = test_search.search_services()
            result_search = test_search.result_search_services()
            assert services == result_search, "Search results don't match"

