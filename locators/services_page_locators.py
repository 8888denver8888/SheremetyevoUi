import random

from selenium.webdriver.common.by import By

class SearchPageLocators:
    SEARCH = (By.CSS_SELECTOR, "input[placeholder='Поиск по услугам в аэропорту']")
    SERVICES = (By.XPATH, f"(//a[@class='ng-star-inserted'])[{random.randint(0, 40)}]")
    RESULT_SEARCH = (By.CSS_SELECTOR, "a[class='ng-star-inserted']")
