from selenium.webdriver.common.by import By

class SearchPageLocators:
    SEARCH = (By.CSS_SELECTOR, "input[dir='auto']")
    CITY = (By.CSS_SELECTOR, "span[class='flight-row__city-name']")

class FlightPageLocators:
    FLIGHT = (By.CSS_SELECTOR, "div[class='row-hover']")
    NOTIFICATION = (By.CSS_SELECTOR, "[class='button ng-star-inserted']")